package org.mik.first.repository;

import org.junit.Assert;
import org.junit.Test;
import org.mik.first.configuration.Config;
import org.mik.first.configuration.Value;
import org.mik.first.configuration.ValueProcessor;

public class ConfigTest {

    class ConfigClient implements ValueProcessor {

        @Value(name = "name", defaultValue = "asd")
        private String name;

        @Value(name = "maxConnections", defaultValue = "50")
        private int maxConnections;

        public ConfigClient() throws Exception {
            Config.initConfig("app.properties");
            processAnnotations();
        }
    }


    @Test
    public void configTest() {
        try {
            ConfigClient cl = new ConfigClient();
            Assert.assertEquals(cl.name, "bsd");
        }
        catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

}
