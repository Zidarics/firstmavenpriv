package org.mik.first.repository;

import org.junit.Assert;
import org.junit.Test;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;

import java.util.List;

public class PersonRepositoryTest {

  private static final String ZAPHOD ="Zaphod Beeblebrox";
  private static final String ZADDRESS ="Betelgeuse";
  private static final String ZAPHOD_ID ="42";

  private static final String FORD = "Ford Prefect";
  private static final String FADDRESS="Earth";
  private static final String F_ID="43";

  private static final String COUNTRY_NAME = "Hungary";
  private static final String COUNTRY_SIGN = "HU";

  @Test
  public void crudTest() throws Exception {
      CountryRepository countryRepository = CountryRepository.getInstance();
      Country country = new Country(COUNTRY_NAME, COUNTRY_SIGN);
      countryRepository.save(country);

      PersonRepository repository = PersonRepository.getInstance();
      Assert.assertEquals(repository.getCount(), 0);

      Person p=new Person(null, ZAPHOD, ZADDRESS, ZAPHOD_ID, country);
      Assert.assertNull(p.getId());
      repository.save(p);
      Assert.assertEquals(repository.getCount(), 1);
      Assert.assertNotNull(p.getId());
      Assert.assertEquals(p.getAddress(), ZADDRESS);
      Assert.assertEquals(p.getName(), ZAPHOD);

      Person byId = repository.getById(p.getId());
      Assert.assertNotNull(p.getId());
      compare(p, byId);

      List<Person> byName = repository.getByName(ZAPHOD);
      Assert.assertEquals(byName.size(),1);
      compare(byName.get(0), p);

      p.setName(FORD);
      p.setIdNumber(F_ID);
      p.setAddress(FADDRESS);
      repository.save(p);

      byId =repository.getById(p.getId());
      compare(byId, p);


      repository.delete(p.getId());
      Assert.assertEquals(repository.getCount(), 0);
  }

  private void compare(Person p1, Person p2) {
    Assert.assertEquals(p1.getAddress(), p2.getAddress());
    Assert.assertEquals(p1.getName(), p2.getName());
  }
}
