<%--
  Created by IntelliJ IDEA.
  User: zamek
  Date: 4/7/20
  Time: 1:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Countries</title>
</head>
<body>
<h1>List of all countries</h1>
<table align="center" cellpadding="1" cellspacing="1" border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Sign</th>
    </tr>
    <c:forEach items="${data}" var="d">
        <tr>
            <td>${d.getId()}</td>
            <td>${d.getName()}</td>
            <td>${d.getSign()}</td>
        </tr>
    </c:forEach>

    <tr>
        <td colspan="5">
        <c:if test="${currentPage gt 0 }">
            <a href="readcountries?recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}">Previous</a>
        </c:if>
        <c:forEach begin="0" end="${numberOfPages-1}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    ${i}
                </c:when>
                <c:otherwise>
                    <a href="readcountries?recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage  lt numberOfPages-1}">
            <a href="readcountries?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}">Next</a>
        </c:if>
        </td>
    </tr>
</table>

</body>
</html>
