<%@ page import="org.mik.first.services.PersonService" %>
<%@ page import="org.mik.first.domain.Person" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: zamek
  Date: 4/7/20
  Time: 1:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    PersonService ps = PersonService.getInstance();
    long cnt = ps.getCounts();
    List<Person> persons = ps.findAll();
%>
<html>
<head>
    <title>List of Persons</title>
</head>
<body>
<h1>List of all persons</h1>
<table align="center" cellpadding="1" cellspacing="1" border="1">
    <tr>
        <th>No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Country</th>
        <th>Identity code</th>
        <th>Created</th>
        <th></th>
    </tr>
    <%
        for(Person p:persons) {
    %>
    <tr>
        <td><%=persons.indexOf(p)%>.</td>
        <td><%=p.getId()%></td>
        <td><%=p.getName()%></td>
        <td><%=p.getCountry().getSign()%></td>
        <td><%=p.getIdNumber()%></td>
        <td><%=p.getCreated()%></td>
    </tr>
    <%
        }
    %>
    <th>
    <td colspan="5"><% out.println("Number of records:"+cnt);%></td>
    </th>
</table>

</body>
</html>
