<%@ page import="org.mik.first.services.CountryService" %>
<%@ page import="org.mik.first.domain.Country" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: zamek
  Date: 4/6/20
  Time: 6:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CountryService cs = CountryService.getInstance();
    long noc = cs.getCounts();
    List<Country> countries = cs.findAll();
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>List of all countries</h1>
    <table align="center" cellpadding="1" cellspacing="1" border="1">
        <tr>
            <th>No.</th>
            <th>ID</th>
            <th>Name</th>
            <th>Sign</th>
        </tr>
<%
    for(Country c:countries) {
%>
     <tr>
         <td><%=countries.indexOf(c)%>.</td>
         <td><%=c.getId()%></td>
         <td><%=c.getName()%></td>
         <td><%=c.getSign()%></td>
     </tr>
<%
    }
%>
    <th>
        <td colspan="5"><% out.println("Number of records:"+noc);%></td>
    </th>
    </table>
</body>
</html>
