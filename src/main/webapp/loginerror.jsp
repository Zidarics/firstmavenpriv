<%--
  Created by IntelliJ IDEA.
  User: zamek
  Date: 5/3/20
  Time: 11:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Error</title>
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
</head>
<body>
<div>
    <h1>Sorry, you are not a registered user</h1>

    <a href="login.jsp">Try again</a>
</div>
</body>
</html>

