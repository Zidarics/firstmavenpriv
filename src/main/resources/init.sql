insert into Country (id, name, sign) values (1000, 'Hungary', 'H');

insert into Country (id, name, sign) values (1001, 'USA', 'US');

insert into Country (id, name, sign) values (1002, 'Russia', 'RU');

insert into Country (id, name, sign) values (1003, 'Ukraine', 'U');

insert into Country (id, name, sign) values (1004, 'Pakistan', 'Pk');

insert into Country (id, name, sign) values (1005, 'Betelgeuse', 'Bt');

insert into Country (id, name, sign) values (1006, 'Earth', 'ETH');

insert into Country (id, name, sign) values (1007, 'Japan', 'JP');

insert into Country (id, name, sign) values (1008, 'Indonesia', 'ID');

insert into Country (id, name, sign) values (1009, 'Nigeria', 'NG');

insert into Country (id, name, sign) values (1010, 'Mexico', 'MX');


insert into client ("id", "name", "address", "country")
values (1000, 'Zaphod Beeblebrox', 'Betelgeuse', 1005);

insert into Person("id", "name", "address", "idNumber", "country")
            values (1000, 'Zaphod Beeblebrox', 'Betelgeuse', '123456789012', 1005);

insert into Person("id", "name", "address", "idNumber", "country")
            select 10001, 'Ford Prefect', 'Betelgeuse', '123456789021', Country.id from Country
                    where Country.sign='Bt';

insert into Person("id", "name", "address", "idNumber", "country")
            select 1002, 'Arthur Dent', 'Earth', '321456789012', Country.id from Country
                    where country.sign='ETH';

insert into Person("id", "name", "address", "idNumber", "country")
            select 1003, 'Tricia McMillan', 'Earth', '543256789012', Country.id from Country
                    where country.sign='ETH';

insert into company("id", "name", "address", "idNumber", "country")
            select 1004, 'Google', 'San Francisco', '13131311313131', Country.id from Country
                    where country.sign='USA';

insert into company("id", "name", "address", "idNumber", "country")
            select 1005, 'Apple', 'Cupertino', '31131311313131', Country.id from Country
                    where country.sign='USA';

insert into company("id", "name", "address", "idNumber", "country")
            select 1006, 'KGB', 'Moscow', '54131311313131', country.id from Country
                    where Country.sign='RU';

commit ;
