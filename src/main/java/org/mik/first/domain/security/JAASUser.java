package org.mik.first.domain.security;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.mik.first.domain.AbstractEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.security.auth.Subject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = JAASUser.TBL_NAME)
public class JAASUser extends AbstractEntity<Long> implements Principal {

    public static final String TBL_NAME = "users";
    public static final String FLD_NAME="name";
    public static final String FLD_PWD="password";
    private static final int COST = 4; // 2^4 iterations

    @Column(name = FLD_NAME, nullable = false)
    @NotNull(message ="Name cannot be null")
    @Size(min = 1, max = 50, message = "Length must be between 1 and 50")
    private String name;

    @Column(name = FLD_PWD, nullable = false)
    @NotNull(message ="Password cannot be null")
    @Size(min = 1, max = 255, message = "Length must be between 1 and 255")
    private String password;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="user_roles",
            joinColumns = {@JoinColumn(name="user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )
    private List<JAASRole> JAASRoles;

    public JAASUser() {}

    public JAASUser(String name, String password) {
        this.name = name;
        this.password = encryptPassword(password);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password!=null)
            this.password = encryptPassword(password);
    }

    public List<JAASRole> getJAASRoles() {
        return JAASRoles;
    }

    public void setJAASRoles(List<JAASRole> JAASRoles) {
        this.JAASRoles = JAASRoles;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    private String encryptPassword(String password) {
        return new String(BCrypt.withDefaults().hash(COST, password.toCharArray()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JAASUser)) return false;
        if (!super.equals(o)) return false;
        JAASUser JAASUser = (JAASUser) o;
        return name.equals(JAASUser.name) &&
                password.equals(JAASUser.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, password);
    }
}
