package org.mik.first.domain.security;

import org.mik.first.domain.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.security.auth.Subject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.Principal;
import java.util.Objects;

@Entity
@Table(name = JAASRole.TBL_NAME)
public class JAASRole extends AbstractEntity<Long> implements Principal {
    public static final String TBL_NAME = "roles";
    public static final String FLD_NAME="name";

    @Column(name = FLD_NAME, nullable = false)
    @NotNull(message ="Name cannot be null")
    @Size(min = 1, max = 50, message = "Length must be between 1 and 50")
    private String name;

    public JAASRole() {}

    public JAASRole(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean implies(Subject subject) {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JAASRole)) return false;
        if (!super.equals(o)) return false;
        JAASRole JAASRole = (JAASRole) o;
        return name.equals(JAASRole.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }
}
