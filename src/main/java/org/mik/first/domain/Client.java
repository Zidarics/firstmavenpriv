package org.mik.first.domain;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Entity(name = Client.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Client extends AbstractEntity<Long> {

    public static final String TBL_NAME="client";
    public static final String FLD_NAME="name";
    public static final String FLD_ADDRESS="address";
    public static final String FLD_COUNTRY="country";

    @Column(name = FLD_NAME, nullable = false, unique = true)
    @NotNull(message = "Name cannot be null")
    @Size(min = 2, max = 50, message = "Name Length must be between 2 and 50")
    @XmlElement
    private String name;

    @Column(name = FLD_ADDRESS, nullable = false)
    @NotNull(message = "Address cannot be null")
    @Size(min = 2, max = 50, message = "Address Length must be between 2 and 50")
    @XmlElement
    private String address;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = FLD_COUNTRY)
    @XmlElement
    private Country country;

    public Client() { }

    public Client(Long id, String name, String address, Country country) {
        super(id);
        this.name = name;
        this.address = address;
        this.country=country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name==null || name.isEmpty() || StringUtils.isBlank(name))
            return;

        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        if (!super.equals(o)) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) &&
                Objects.equals(address, client.address) &&
                Objects.equals(country, client.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, address, country);
    }

    @Override
    public String toString() {
        return "Client{" +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
