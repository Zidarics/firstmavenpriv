package org.mik.first.domain;


import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 */

//@XMLSerializable
//@JSonSerializable
@Entity(name = Person.TBL_NAME)
public class Person extends Client  {

    public static final String TBL_NAME = "Person";
    public static final String FLD_PERSONAL_ID="idNumber";

//    @XMLElement(key = "PersonalId")
//    @JSonElement
    @Column(name = FLD_PERSONAL_ID, nullable = false, unique =  true)
    private String idNumber;

    /**
     *
     */
    public Person() {
        super();
    }

    /**
     * Constructor for Person with idNumber only
     *
     * @param idNumber person iud number it cannot be null!
     */
    public Person(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * Constructor for Person with all fields
     *
     * @param id can be null
     * @param name cannot be null
     * @param address cannot be null
     * @param idNumber user's personal id for governement, it can't be null
     * @Param country user's country
     */
    public Person(Long id, String name, String address, String idNumber, Country country) {
        super(id, name, address, country);
        this.idNumber = idNumber;
    }


    /**
     * get the person Personal ID Number
     * @return id it can't be null
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     *
     * @param idNumber cannot be null
     */
    public void setIdNumber(String idNumber) {
        if (idNumber != null && !idNumber.isEmpty())
            this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        if (!super.equals(o)) return false;

        Person person = (Person) o;

        return idNumber.equals(person.idNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + idNumber.hashCode();
        return result;
    }

    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                super.toString() +
                '}' ;
    }
}
