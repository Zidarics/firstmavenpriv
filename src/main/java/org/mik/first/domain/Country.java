package org.mik.first.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.mik.first.export.json.JSonSerializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Objects;

@Entity(name = Country.TBL_NAME)
@JSonSerializable
@SequenceGenerator(name = "generator", sequenceName = "country_seq", allocationSize = 1)
@XmlRootElement(name = Country.TBL_NAME)
public class Country extends AbstractEntity<Long>{

    public static final String TBL_NAME="Country";
    public static final String FLD_NAME="name";
    public static final String FLD_SIGN="sign";

    @Column(name = FLD_NAME, nullable = false)
    @NotNull(message ="Name cannot be null")
    @Size(min = 1, max = 50, message = "Length must be between 1 and 50")
    @XmlElement
    private String name;

    @Column(name = FLD_SIGN, nullable = false)
    @NotNull(message = "Sign cannot be null")
    @Size(min = 1, max = 5, message = "Length must be between 1 and 5")
    @XmlElement
    private String sign;

    @JsonIgnore
    @OneToMany(mappedBy = Client.FLD_COUNTRY, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Client> clients;

    public Country() {}

    public Country(String name, String sign) {
        super();
        this.name = name;
        this.sign = sign;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        if (!super.equals(o)) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) &&
                Objects.equals(sign, country.sign) &&
                Objects.equals(clients, country.clients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, sign, clients);
    }

    @Override
    public String toString() {
        return String.format("%s(%s)", name, sign);
    }

}
