package org.mik.first.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = Company.TBL_NAME)
public class Company extends Client {
    public static final String TBL_NAME = "Company";
    public static final String FLD_TAX_NUMBER ="tax_number";

    @Column(name = FLD_TAX_NUMBER, nullable = false, unique = true)
    private String taxNumber;

    public Company() {
    }

    public Company(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public Company(Long id, String name, String address, String taxNumber, Country country) {
        super(id, name, address, country);
        this.taxNumber = taxNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;

        Company company = (Company) o;

        return taxNumber.equals(company.taxNumber);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + taxNumber.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' +
                super.toString() +
                '}';
    }
}
