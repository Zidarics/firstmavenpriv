package org.mik.first;

import org.mik.first.domain.Client;
import org.mik.first.domain.Person;

public class Main {

    public static void main(String[] main) {
        new Control().start();
    }
}
