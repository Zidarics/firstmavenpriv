package org.mik.first.jaas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

public class JAASCallbackHandler implements CallbackHandler {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private String userName;
    private String password;

    public JAASCallbackHandler(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }


    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        LOG.debug("Callback handler invoked");

        for(int i=0; i<callbacks.length; ++i) {
            if (callbacks[i] instanceof NameCallback) {
                NameCallback nc = (NameCallback) callbacks[i];
                nc.setName(this.userName);
                continue;
            }
            if (callbacks[i] instanceof PasswordCallback) {
                PasswordCallback pc = (PasswordCallback) callbacks[i];
                pc.setPassword(password.toCharArray());
                continue;
            }
            throw new UnsupportedCallbackException(callbacks[i], "The submitted callback is unsupported");
        }

    }
}
