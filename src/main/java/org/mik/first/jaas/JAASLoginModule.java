package org.mik.first.jaas;


/*
 * Start tomcat in Services!
 * Edit/configuration/Startup-> Environment variable: CATALINA_OPTS="-Djava.security.auth.login.config\=/opt/tomcat/conf/jaas.config"
 */

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.security.JAASUser;
import org.mik.first.repository.UserRepository;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class JAASLoginModule implements LoginModule {
    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private CallbackHandler callbackHandler;
    private Subject subject;
    private JAASUser user;
    private Map sharedState;
    private Map options;
    private boolean succeeded = false;
    private boolean commitSucceeded = false;

    private String userName;
    private String password;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler,
                           Map<String, ?> sharedState, Map<String, ?> options) {
        this.callbackHandler = callbackHandler;
        this.subject = subject;
        this.sharedState = sharedState;
        this.options = options;
    }

    @Override
    public boolean login() throws LoginException {
        Callback[] callbacks = new Callback[2];
        callbacks[0]=new NameCallback("login");
        callbacks[1]=new PasswordCallback("password", true);
        UserRepository userRepository = UserRepository.getInstance();

        try {
            callbackHandler.handle(callbacks);
            userName = ((NameCallback) callbacks[0]).getName();
            password = String.valueOf(((PasswordCallback) callbacks[1]).getPassword());
            List<JAASUser> users = userRepository.getByName(userName);
            if (users==null || users.isEmpty())
                return false;

            Optional<JAASUser> usr = users.stream()
                                        .filter(u-> BCrypt.verifyer().verify(password.toCharArray(), u.getPassword()).verified)
                                        .findFirst();

            if (usr ==null)
                return  false;

            this.user = usr.get();
            succeeded = true;
            return true;
        }
        catch (Exception e) {
            LOG.warn(e);
            throw new LoginException(e.getMessage());
        }
    }

    @Override
    public boolean commit() throws LoginException {
        if (!(succeeded && (this.user != null)))
            return false;

        if (!subject.getPrincipals().contains(this.user)) {
            this.subject.getPrincipals().add(this.user);
            LOG.debug("User principal added:"+this.user);
        }

        this.user.getJAASRoles().forEach(this.subject.getPrincipals()::add);

        commitSucceeded = true;
        LOG.info("Login subject were successfully populated with principals and roles");
        return true;
    }

    @Override
    public boolean abort() throws LoginException {
        if (!succeeded)
            return false;

        if (!commitSucceeded) {
            succeeded = false;
            user = null;
            userName = null;
            password = null;
        }
        else
            logout();

        return true;
    }

    @Override
    public boolean logout() throws LoginException {
        subject.getPrincipals().clear();
        succeeded = false;
        user=null;
        this.userName=null;
        StringUtils.leftPad(password, password.length(), ' ');
        return false;
    }

}
