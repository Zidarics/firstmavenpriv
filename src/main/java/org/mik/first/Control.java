package org.mik.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;
import org.mik.first.services.CompanyService;
import org.mik.first.services.PersonService;


import java.util.ArrayList;
import java.util.List;

public class Control {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = true;

    private final PersonService personService;
    private final CompanyService companyService;

    private long id;

    public Control() {
        this.personService = PersonService.getInstance();
        this.companyService = new CompanyService();
        this.id=0;
    }

    public void start() {
        List<String[]> dummyList = createDummyList();
        Client client;
        for (String[] strings: dummyList) {
            client=this.convertFromString(strings);
            this.dewIt(client);
        }
    }

    private Client convertFromString(String[] strings) {
        switch (strings[0]) {
           case  "P" : return new Person(id++, strings[1], strings[2], strings[3], new Country(strings[4], strings[5]));
           case  "C" : return new Company(id++, strings[1], strings[2], strings[3], new Country(strings[4], strings[5]));
            default: if (DEBUG_TEMPORARY)
               LOG.debug("Control.convertFromString error: unknown flag "+strings[0]);
               throw  new RuntimeException("Control.convertFromString error: unknown flag"+strings[0]);
        }
    }

    private void dewIt(Client client) {
        if (client instanceof Person) {
            this.personService.pay((Person) client);
            return;
        }
        if (client instanceof Company) {
            this.companyService.pay((Company) client);
            return;
        }
        throw  new RuntimeException("Unknown client");
    }

    private List<String[]> createDummyList() {
        List<String[]> dummyList = new ArrayList<>();
        dummyList.add(new String[] { "P", "Zaphod Beeblebrox", "Betelgeuse", "42234560TA", "Hungary", "HU"});
        dummyList.add(new String[] { "P", "Linus Torvalds", "USA", "42234560TL", "Hungary", "HU"});
        dummyList.add(new String[] { "P", "Tricia McMillan", "Earth", "42234560TM", "Hungary", "HU"});
        dummyList.add(new String[] { "P", "Ford Prefect", "Betelgeuse", "42234560TB", "Hungary", "HU"});
        dummyList.add(new String[] { "C", "Google", "USA", "43234560TL", "USA", "US"});
        dummyList.add(new String[] { "C", "Oracle", "USA", "43234560TO", "USA", "US"});
        dummyList.add(new String[] { "C", "Microsoft", "USA", "4344566560TL", "USA", "US"});
        dummyList.add(new String[] { "C", "KGB", "Russia", "43134560TL", "USA", "US"});

        return dummyList;
    }
}
