package org.mik.first.servlet.db;

import org.mik.first.domain.AbstractEntity;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AbstractDbServlet<T extends AbstractEntity<Long>> extends HttpServlet {

    public static final String KEY_CURRENT_PAGE = "currentPage";
    public static final String KEY_RECORDS_PER_PAGE = "recordsPerPage";
    public static final String KEY_NUMBER_OF_PAGES = "numberOfPages";

    public static final String KEY_DATA = "data";

    public static final int DEF_RECORDS_PER_PAGES = 5;

    protected void setContentType(HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
    }

    protected int extractIntRequestparam(HttpServletRequest request, String paramName, int defaultValue) {

        try {
            return Integer.parseInt(request.getParameter(paramName));
        }
        catch (Exception e) {
            return defaultValue;
        }
    }

    protected void setPagerData(HttpServletRequest request, List<T> data, long maxCounts, int recordsPerPage, int currentPage) {
        request.setAttribute(KEY_DATA, data);
        long numberOfPages = maxCounts / recordsPerPage;
        if (maxCounts % recordsPerPage > 0)
            ++numberOfPages;
        request.setAttribute(KEY_NUMBER_OF_PAGES, numberOfPages);
        request.setAttribute(KEY_CURRENT_PAGE, currentPage);
        request.setAttribute(KEY_RECORDS_PER_PAGE, recordsPerPage);

    }
}
