package org.mik.first.servlet.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Country;
import org.mik.first.repository.CountryRepository;
import org.mik.first.servlet.rest.helper.ObjectList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * REST = REpresentational State Transfer
 *
 * https://dennis-xlc.gitbooks.io/restful-java-with-jax-rs-2-0-en/cn/part1/chapter7/complex_responses.html
 * WADL Web Application Description Language
 * http :8888/rest/application.wadl
 * http :8888/rest/application.wadl?detail=true
 *
 * http :8888/rest/countries/all
 * http :8888/rest/countries/1
 * http POST :8888/rest/countries name="asdf" sign="a"
 * http PUT :8888/rest/countries/1 name="asdf" sign="a"
 * http DELETE :8888/rest/countries/1
 *
 */
@Path(CountryService.BASE_PATH)
public class CountryService {
    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = true;

    public static final String BASE_PATH = "/countries";

    private CountryRepository countryRepository = CountryRepository.getInstance();

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CountryService.getAll ");
        try {
            ObjectList<Country> countries = new ObjectList<>(this.countryRepository.findAll());
            return Response.ok(countries, MediaType.APPLICATION_JSON).build();
        }
        catch (Exception e) {
            LOG.error(e);
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Long id) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CountryService.get id:"+id);

        try {
            Country c = this.countryRepository.getById(id);
            return c!=null
                    ? Response.ok(c, MediaType.APPLICATION_JSON).build()
                    : Response.status(Response.Status.NOT_FOUND).build();
        }
        catch (Exception e) {
            LOG.error(e);
            return Response.noContent().build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(Country c) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CountryService.add");

        try {
            this.countryRepository.saveOrupdate(c);
            URI uri = new URI(String.format("%s/%d",BASE_PATH, c.getId()));
            return Response.created(uri).build();
        }
        catch (Exception e) {
            LOG.error(e);
            return Response.serverError().build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Country country) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CountryService.update, id:"+id);

        try {
            Country c1 = this.countryRepository.getById(id);
            c1.setName(country.getName());
            c1.setSign(country.getSign());
            this.countryRepository.update(c1);
            return Response.ok(id).build();
        }
        catch (Exception e) {
            LOG.error(e);
            return Response.notModified(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter CountryService.delete id:"+id);

        try {
            this.countryRepository.delete(id);
            return Response.ok().build();
        }
        catch (Exception e) {
            LOG.error(e);
            return Response.notModified(e.getMessage()).build();
        }
    }

}
