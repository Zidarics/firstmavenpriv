package org.mik.first.servlet.rest.helper;

import java.util.List;

public class ObjectList<T> {

    private List<T> data;

    public ObjectList(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
