package org.mik.first.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/formServlet")
public class FormServlet extends HttpServlet {

    public FormServlet() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String yourName = httpServletRequest.getParameter("your_name");
        PrintWriter writer = httpServletResponse.getWriter();
        writer.println("<h1>Hello "+yourName+"</h1>");
        writer.close();
    }
}

