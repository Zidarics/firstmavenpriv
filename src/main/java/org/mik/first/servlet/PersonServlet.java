package org.mik.first.servlet;

import org.mik.first.domain.Country;
import org.mik.first.domain.Person;
import org.mik.first.services.CountryService;
import org.mik.first.services.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.stream.Stream;

public class PersonServlet extends HttpServlet {

    private PersonService personService;
    private CountryService countryService;

    public PersonServlet() {
        this.personService = PersonService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession();
        String name = Optional.ofNullable(req.getCookies())
                                .map(Stream::of)
                                .orElseGet(Stream::empty)
                                .filter(c->"name".equals(c.getName()))
                                .findFirst()
                                .map(Cookie::getValue)
                                .map(URLDecoder::decode)
                                .orElse(null);
        this.handle(resp, name);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String name = req.getParameter("name");
            String address = req.getParameter("address");
            String personalId = req.getParameter("pId");
            String countryId = req.getParameter("cId");

            Country country = this.countryService.findById(countryId);
            if (country==null) {
                resp.sendRedirect("Error");
                return;
            }

            Person person = new Person(null, name, address, personalId, country);
            this.personService.pay(person);

            Cookie cookie = new Cookie("name", URLEncoder.encode(name, "UTF-8"));
            resp.addCookie(cookie);
            resp.sendRedirect("clientList");
        }
        catch (Exception e) {
            resp.sendRedirect("Error");
        }
    }

    private void handle(HttpServletResponse response, String name) throws IOException {
        PrintWriter writer = response.getWriter();
        writer.println("<html><head></head><body>");
        if (name!=null)
            writer.println("<h1>Hello " + name + "!</h1>");
        writer.println("<form method=\"POST\">");
        writer.println("Name: <input type=\"text\" name=\"name\" />");
        writer.println("Address: <input type=\"text\" name=\"address\" />");
        writer.println("Personal ID: <input type=\"text\" name=\"pId\" />");
        writer.println("Country id: <input type=\"text\" name=\"cId\" />");
        writer.println("<input type=\"submit\"/>");
        writer.println("</form>");
        writer.println("</body>");
        writer.println("</html>");
    }

    private void printHtmlHeader(PrintWriter writer) {
        writer.println("<html><head></head><body>");
    }

}
