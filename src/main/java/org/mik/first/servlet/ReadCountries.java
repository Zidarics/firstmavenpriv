package org.mik.first.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Country;
import org.mik.first.services.CountryService;
import org.mik.first.servlet.db.AbstractDbServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ReadCountries", urlPatterns = {"/readcountries"})
public class ReadCountries extends AbstractDbServlet<Country> {
    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private CountryService countryService = CountryService.getInstance();
    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        try {
            setContentType(httpServletResponse);
            int currentPage = extractIntRequestparam(httpServletRequest, KEY_CURRENT_PAGE, 0);
            int recordsPerPage = extractIntRequestparam(httpServletRequest, KEY_RECORDS_PER_PAGE, DEF_RECORDS_PER_PAGES);
            List<Country> countries = countryService.findAll(currentPage, recordsPerPage);
            long maxCount = countryService.getCounts();
            setPagerData(httpServletRequest, countries, maxCount, recordsPerPage, currentPage);
            RequestDispatcher dispatcher = httpServletRequest.getRequestDispatcher("listCountries.jsp");
            dispatcher.forward(httpServletRequest, httpServletResponse);
        }
        catch (Exception e) {
            LOG.error(e);
        }
    }

}
