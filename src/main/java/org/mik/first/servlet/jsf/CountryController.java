package org.mik.first.servlet.jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Country;
import org.mik.first.services.CountryService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

@SessionScoped
@ManagedBean
public class CountryController extends AbstractController<Country> {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;


    private CountryService countryService;
    private String name;
    private String sign;

    public CountryController() {
        this.countryService = CountryService.getInstance();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    protected List<Country> findAll(int currentPage, int rowsPerPage) throws Exception{
        return this.countryService.findAll(currentPage, rowsPerPage);
    }

    @Override
    protected long getCounts() throws Exception {
        return this.countryService.getCounts();
    }

    public String delete(long id) {
        try {
            this.countryService.delete(id);
            return toUrl("countries.xhtml",true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public String update(long id) {
        try {
            Country c=this.countryService.findById(id);
            if (c==null)
                return null;
            Map<String, Object> sessionMapObject = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMapObject.put("editRecord", c);
            return toUrl("api/editcountry.xhtml",true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public String save(Country country) {
        try {
            this.countryService.save(country);
            return toUrl("countries.xhtml", true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public String save(CountryController countryController) {
        try {
            Country country = new Country(countryController.name, countryController.sign);
            this.countryService.save(country);
            return toUrl("countries.xhtml", true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public String updateCountry(Country country) {
        try {
            this.countryService.update(country);
            return toUrl("countries.xhtml", true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }
}
