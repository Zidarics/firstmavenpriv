package org.mik.first.servlet.jsf;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "helloWorld", eager = true)
public class HelloWorld {
    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    public HelloWorld() {
        LOG.info("HelloWorld started");
    }

    public String getMessage() {
        return "Hello World";
    }
}
