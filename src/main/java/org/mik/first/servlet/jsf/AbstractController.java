package org.mik.first.servlet.jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.AbstractEntity;

import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractController<T extends AbstractEntity<Long>> implements Serializable {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = true;

    public static final int DEFAULT_ROWS_PER_PAGE = 5;
    public static final int DEFAULT_PAGE_RANGE = 10;

    private List<T> countryList;
    private long totalRows;
    private int rowsPerPage=DEFAULT_ROWS_PER_PAGE;
    private int totalPages;
    private int currentPage;
    private List<Integer> pages=new ArrayList<>();


    protected abstract List<T> findAll(int currentPage, int rowsPerPage) throws Exception;

    protected abstract long getCounts() throws Exception;

    public List<T> getDataList() throws Exception {
        if (this.countryList == null)
            loadDataList();
        return this.countryList;
    }

    public void setDataList(List<T> list) {
        this.countryList = list;
    }

    public long getTotalRows() {
        return totalRows;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        if (rowsPerPage<totalRows)
            this.rowsPerPage = rowsPerPage;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void page(int page) {
        if (page<0 && page >= totalPages)
            return;
        currentPage = page;
        loadDataList();
    }

    public List<Integer> getPages() {
        return pages;
    }

    private void loadDataList() {
        try {
            this.countryList = findAll(this.currentPage, this.rowsPerPage);
            this.totalRows = getCounts();
            this.totalPages = Long.valueOf(totalRows % this.rowsPerPage == 0 ? totalRows / rowsPerPage : totalRows/rowsPerPage + 1).intValue();
            this.pages.clear();
            for(int i=0;i<totalPages;++i)
                pages.add(i);
        }
        catch (Exception e) {
            LOG.error(e);
        }
    }

    public void page(ActionEvent event) {
        page(((Integer) ((UICommand) event.getComponent()).getValue()));
    }

    public void pageFirst() {
        page(0);
    }

    public void pageNext() {
        if (currentPage<totalPages)
            page(currentPage+1);
    }

    public void pagePrev() {
        if (currentPage>0)
            page(currentPage-1);
    }

    public void pageLast() {
        page(totalPages-1);
    }

    public String toUrl(String url, boolean redirect) {
        return String.format("/%s?faces-redirect=%s", url, redirect?"true":"false");
    }
}
