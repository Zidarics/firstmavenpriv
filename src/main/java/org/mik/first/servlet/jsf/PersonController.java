package org.mik.first.servlet.jsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;
import org.mik.first.services.CountryService;
import org.mik.first.services.PersonService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@SessionScoped
@ManagedBean
public class PersonController extends AbstractController<Person> {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private PersonService personService=PersonService.getInstance();
    private CountryService countryService = CountryService.getInstance();

    private String name;
    private String address;
    private Long countryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long country) {
        this.countryId = country;
    }

    public List<Country> getCountries() {
        try {
            return this.countryService.findAll();
        }
        catch (Exception e) {
            LOG.error(e);
            return Collections.EMPTY_LIST;
        }
    }

    public String update(Long id) {
        try {
             Person p = this.personService.findById(id);
             if (p==null)
                 return null;

            Map<String, Object> sessionMapObject = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            sessionMapObject.put("editRecord", p);
            return toUrl("editperson.xhtml",true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public String updatePerson(Person p) {
        try {
            Country c = this.countryService.findById(this.countryId);
            p.setCountry(c);
            this.personService.update(p);
            return toUrl("persons", true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    @Override
    protected List<Person> findAll(int currentPage, int rowsPerPage) throws Exception {
        return personService.findAll();
    }

    @Override
    protected long getCounts() throws Exception {
        return personService.getCounts();
    }

    public String delete(long id) {
        try {
            this.personService.delete(id);
            return toUrl("persons.xhtml", true);
        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }
}
