package org.mik.first.servlet.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.Principal;

@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController implements Serializable {
    private static final long serialVersionUID = 1L;
    @ManagedProperty(value = "#{param.pageId}")
    private String pageId;

    public String moveToPage1() {
        return "countries";
    }

    public String moveToPage2() {
        return "page2";
    }

    public String moveToHomePage() {
        return "home";
    }

    public String processPage1() {
        return "page";
    }

    public String processPage2() {
        return "page";
    }

    public String showPage() {
        if(pageId == null)
            return "home";

        if(pageId.equals("1"))
            return "countries";

        if(pageId.equals("2"))
            return "persons";

        if (pageId.equals("99")) {
            logout();
            return "/home?faces-redirect=true";
        }
        return "home";
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    private void logout() {
        FacesContext facesContext =FacesContext.getCurrentInstance();
        facesContext.getExternalContext().invalidateSession();

    }

    public boolean isLogoutRendered() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Principal p=request.getUserPrincipal();
        return p!=null;
    }

}
