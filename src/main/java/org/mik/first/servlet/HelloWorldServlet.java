package org.mik.first.servlet;

import org.mik.first.Control;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldServlet extends HttpServlet {

    private Control control;

    public HelloWorldServlet() {
        this.control = new Control();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.control.start();
        PrintWriter writer = resp.getWriter();

        writer.println("Done!");
    }
}
