package org.mik.first.export.json;

public class JSonSerializationException extends RuntimeException {

    public JSonSerializationException() {
    }

    public JSonSerializationException(String message) {
        super(message);
    }

    public JSonSerializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JSonSerializationException(Throwable cause) {
        super(cause);
    }

    public JSonSerializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
