package org.mik.first.export.xml;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class XMLGenerator {
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public String convert2XML(Object object) throws XMLSerializationException {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(LINE_SEPARATOR).append("<?xml version=\"1.0\" ?>").append(LINE_SEPARATOR);
            process(object, sb);
            return sb.toString();
        }
        catch (Exception e) {
            throw new XMLSerializationException(e.getMessage(), e);
        }
    }

    private void process(Object object, StringBuilder sb) throws
                IllegalArgumentException, IllegalAccessException,
                InvocationTargetException {

        checkIfSerializable(object);
        initXML(object);
        convert2String(object, sb);
    }

    private void checkIfSerializable(Object object) {
        if (object==null)
            throw new XMLSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        if (!clazz.isAnnotationPresent(XMLSerializable.class))
            throw new XMLSerializationException(clazz.getName() +
                    " object cannot serializable to XML");
    }

    private void initXML(Object object) throws IllegalAccessException, InvocationTargetException {
        if (object==null)
            throw new XMLSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        List<Method> methods =new ArrayList<>();
        Collections.addAll(methods, clazz.getDeclaredMethods());
        for (Method m:methods) {
            if (m.isAnnotationPresent(XMLInit.class)) {
                m.setAccessible(true);
                m.invoke(object);
            }
        }
    }

    private void convert2String(Object object, StringBuilder sb)
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (object==null)
            throw new XMLSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        XMLSerializable xmlSerializable = clazz.getAnnotation(XMLSerializable.class);
        String classKey = "".equals(xmlSerializable.key())
                ? clazz.getSimpleName().toUpperCase()
                : xmlSerializable.key();
        sb.append('<').append(classKey).append('>').append(LINE_SEPARATOR);
        List<Field> fields = new ArrayList<>();
        Collections.addAll(fields, clazz.getDeclaredFields());
        addParentFields(clazz, fields);
        for(Field f:fields) {
            if (f.isAnnotationPresent(XMLElement.class)) {
                f.setAccessible(true);
                XMLElement element = f.getAnnotation(XMLElement.class);
                String key = "".equals(element.key())
                        ? f.getName().toUpperCase()
                        : element.key();
                Object value = f.get(object);
                sb.append('<').append(key).append('>').append(LINE_SEPARATOR);
                if (value!=null && value.getClass()
                        .isAnnotationPresent(XMLSerializable.class)) {
                    sb.append(LINE_SEPARATOR);
                    process(value, sb);
                }
                else {
                    sb.append(value);
                }
                sb.append(LINE_SEPARATOR).append("</").append(key).append('>').append(LINE_SEPARATOR);
            }
        }
        sb.append("</").append(classKey).append('>').append(LINE_SEPARATOR);
    }

    private void addParentFields(Class<?> clazz, List<Field> fields) {
        Class<?> superClass = clazz.getSuperclass();
        if (superClass.isAnnotationPresent(XMLSerializable.class)) {
            Collections.addAll(fields, superClass.getDeclaredFields());
            addParentFields(superClass, fields);
        }
    }
}
