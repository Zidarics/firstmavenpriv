package org.mik.first.configuration;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;

import java.io.File;

public class Config {

    private Configurations configs = new Configurations();
    private static Configuration config;

    private Config(String filename) throws Exception {
        config = configs.properties(new File(filename));
    }

    public static void initConfig(String fileName) throws Exception {
        new Config(fileName);
    }

    public static synchronized Configuration getConfig() {
        return config;
    }

}
