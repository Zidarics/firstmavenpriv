package org.mik.first.configuration;

import org.apache.commons.configuration2.Configuration;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface ValueProcessor {

    default void processAnnotations() throws IllegalAccessException {
        Class<?> cl = getClass();
        List<Field> fields = new ArrayList<>();
        Collections.addAll(fields, cl.getDeclaredFields());
        addParentFields(cl, fields);
        Configuration conf = Config.getConfig();
        for(Field f:fields) {
            if (!f.isAnnotationPresent(Value.class))
                continue;

            Value an = f.getAnnotation(Value.class);
            String def = an.defaultValue();

            f.setAccessible(true);

            if(f.getType()==String.class) {
                f.set(this, conf.getString(an.name(), (!def.isEmpty()) ? def : null));
                continue;
            }

            if(f.getType()==Integer.class || f.getType()==int.class) {
                f.set(this, conf.getInt(an.name(), (!def.isEmpty()) ? Integer.parseInt(def) : 0));
                continue;
            }

            if(f.getType()==Float.class || f.getType()==float.class) {
                f.set(this, conf.getFloat(an.name(), (!def.isEmpty()) ? Float.parseFloat(def) : 0.0f));
                continue;
            }

            if(f.getType()==Long.class || f.getType()==long.class) {
                f.set(this, conf.getLong(an.name(), (!def.isEmpty()) ? Long.parseLong(def) : 0));
                continue;
            }

            if(f.getType()==Double.class || f.getType()==double.class) {
                f.set(this, conf.getDouble(an.name(), (!def.isEmpty()) ? Double.parseDouble(def) : 0.0));
                continue;
            }

            if (f.getType()==char.class || f.getType()==Character.class) {
                f.set(this, conf.getByte(an.name(), (!def.isEmpty()) ? (byte) def.charAt(0) : 0));
                continue;
            }

            if(f.getType()==Short.class || f.getType()==short.class) {
                f.set(this, conf.getShort(an.name(), (!def.isEmpty()) ? Short.parseShort(def) : 0));
            }

        }
    }

    static void addParentFields(Class<?> cl, List<Field> fields) {

    }
}
