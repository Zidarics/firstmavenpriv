package org.mik.first.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.mik.first.domain.Client;
import org.mik.first.domain.Country;

public class CountryRepository extends AbstractRepository<Long, Country> {

    private final static Logger LOG  = LogManager.getLogger();

    private static CountryRepository instance;


    private CountryRepository() {}

    public Country getBySign(String sign) throws Exception {
        if (sign==null || sign.isEmpty())
            return null;
        return doIt(((session, tr) -> {
            Query<Country> query = createCriteria(session, getClazz(), (cb, r, cq) -> {
                cq.select(r).where(cb.equal(r.get(Country.FLD_SIGN),sign));
            });
            return query.getSingleResult();
        }));
    }

    @Override
    protected Class<Country> getClazz() {
        return Country.class;
    }

    public static synchronized CountryRepository getInstance() {
        if (instance==null) {
            instance = new CountryRepository();
        }

        return instance;
    }

}



