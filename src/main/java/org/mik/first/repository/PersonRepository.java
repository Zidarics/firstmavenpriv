package org.mik.first.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.mik.first.domain.Person;

public class PersonRepository extends AbstractRepository<Long, Person> {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static PersonRepository instance=null;

    private PersonRepository() {}


    @Override
    protected Class<Person> getClazz() {
        return Person.class;
    }

    public static synchronized PersonRepository getInstance() {
        if (instance==null) {
            instance = new PersonRepository();
        }

        return instance;
    }

    public Person getByPersonalId(String id) throws Exception {
        if (id==null || id.isEmpty())
            return null;
        return doIt(((session, tr) -> {
            Query<Person> query = createCriteria(session, getClazz(), (cb, r, cq) -> {
                cq.select(r).where(cb.equal(r.get(Person.FLD_PERSONAL_ID), id));
            });
            return query.getSingleResult();
        }));
    }

}
