package org.mik.first.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;
import org.mik.first.domain.security.JAASUser;

public class UserRepository extends AbstractRepository<Long, JAASUser> {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private static UserRepository instance;

    private UserRepository() {}

    public JAASUser authenticate(String userName, String password) throws Exception {
        if (userName==null || userName.isEmpty() || password==null || password.isEmpty())
            return null;

        return doIt(((session, tr) -> {
            Query<JAASUser> query = createCriteria(session, JAASUser.class, (cb, r, cq) -> {
                cq.select(r).where(
                        cb.and(
                            cb.equal(r.get(JAASUser.FLD_NAME), userName),
                            cb.equal(r.get(JAASUser.FLD_PWD), password)
                        ));
            });
            return query.getSingleResult();
        }));
    }

    @Override
    protected Class<JAASUser> getClazz() {
        return JAASUser.class;
    }

    public static synchronized UserRepository getInstance() {
        if (instance==null)
            instance = new UserRepository();

        return instance;
    }
}
