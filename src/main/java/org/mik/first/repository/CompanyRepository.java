package org.mik.first.repository;

import org.hibernate.query.Query;
import org.mik.first.domain.Company;

public class CompanyRepository extends AbstractRepository<Long, Company> {

    private static CompanyRepository instance;

    private CompanyRepository() {}

    public Company getByTaxNumber(String taxNumber) throws  Exception {
        if (taxNumber==null || taxNumber.isEmpty())
            return null;

        return doIt(((session, tr) -> {
            Query<Company> query = createCriteria(session, getClazz(), (cb, r, cq) -> {
                cq.select(r).where(cb.equal(r.get(Company.FLD_TAX_NUMBER), taxNumber));
            });
            return query.getSingleResult();
        }));
    }

    public synchronized static CompanyRepository getInstance() {
        if (instance==null) {
            instance=new CompanyRepository();
        }
        return instance;
    }

    @Override
    protected Class<Company> getClazz() {
        return Company.class;
    }

}
