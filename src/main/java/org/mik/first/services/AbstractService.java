package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.AbstractEntity;
import org.mik.first.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity<Long>> {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    protected abstract AbstractRepository<Long, T> getRepository();

    public T findById(String id) throws Exception {
        return getRepository().getById(Long.parseLong(id));
    }

    public T findById(long id) throws Exception {
        return  getRepository().getById(id);
    }

    public List<T> findAll() throws Exception {
        return this.getRepository().findAll();
    }

    public long getCounts() throws Exception {
        return getRepository().getCount();
    }

    public List<T> getByName(String name) throws Exception {
        return getRepository().getByName(name);
    }

    public List<T> findAll(int currentPage, int recordsPerPage) throws  Exception {
        return  getRepository().findAll(currentPage, recordsPerPage);
    }

    public void delete(long id) throws Exception {
        getRepository().delete(id);
    }

    public void update(long id) throws Exception{
        T c = getRepository().getById(id);
        if (c==null) {
            LOG.error(String.format("Object with id:%d not found", id));
            return;
        }
        getRepository().update(c);
    }

    public void save(T t) throws Exception {
        getRepository().save(t);
    }

    public void update(T t) throws Exception {
        getRepository().update(t);
    }

}
