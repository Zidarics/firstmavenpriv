package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Country;
import org.mik.first.repository.AbstractRepository;
import org.mik.first.repository.CountryRepository;

public class CountryService extends AbstractService<Country> {

    private static final Logger LOG  = LogManager.getLogger();

    private static final Boolean DEBUG_TEMPORARY = false;
    private static CountryService instance;

    private CountryRepository countryRepository;

    private CountryService() {
        this.countryRepository = CountryRepository.getInstance();
    }

    public static synchronized CountryService getInstance() {
        if (instance==null)
            instance = new CountryService();

        return instance;
    }

    @Override
    protected AbstractRepository<Long, Country> getRepository() {
        return this.countryRepository;
    }
}
