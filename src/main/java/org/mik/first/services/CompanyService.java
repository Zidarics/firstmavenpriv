package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Company;
import org.mik.first.export.json.JSonGenerator;
import org.mik.first.export.xml.XMLGenerator;

public class CompanyService implements Service<Company> {

    private final static Boolean DEBUG_TEMPORAY = true;

    private final static Logger LOG = LogManager.getLogger();

    private XMLGenerator xmlGenerator = new XMLGenerator();
    private JSonGenerator jsonGenerator = new JSonGenerator();

    @Override
    public void pay(Company client) {
        if (DEBUG_TEMPORAY)
            LOG.debug("Enter CompanyService.pay ");
        System.out.println("Enter CompanyService.pay " + client);
        System.out.println(xmlGenerator.convert2XML(client));
        System.out.println(jsonGenerator.convert2JSon(client));
    }

    @Override
    public void receiveService(Company client) {
        LOG.info("Enter receiveService" + client);
        System.out.println("Enter receiveService, client:"+client);
        System.out.println(xmlGenerator.convert2XML(client));
        System.out.println(jsonGenerator.convert2JSon(client));
    }
}
