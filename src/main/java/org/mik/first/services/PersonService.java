package org.mik.first.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Person;
import org.mik.first.repository.AbstractRepository;
import org.mik.first.repository.PersonRepository;

public class PersonService extends AbstractService<Person> implements Service<Person> {

    private static PersonService instance;


    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;
    private PersonRepository personRepository;

    private PersonService() {
        this.personRepository = PersonRepository.getInstance();
    }

    public static synchronized PersonService getInstance() {
        if (instance==null)
            instance = new PersonService();
        return instance;
    }

    @Override
    public void pay(Person client) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter PersonService. client:"+client);
        System.out.println("Enter PersonService. client:"+client);
    }

    @Override
    public void receiveService(Person client) {
        if (DEBUG_TEMPORARY)
           LOG.debug("Enter PersonService.receiveService client:" + client);
        System.out.println("Enter PersonService.receiveService client:" + client);
    }

    @Override
    protected AbstractRepository<Long, Person> getRepository() {
        return this.personRepository;
    }
}
