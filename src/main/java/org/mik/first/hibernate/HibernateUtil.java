package org.mik.first.hibernate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Country;
import org.mik.first.domain.Person;
import org.mik.first.domain.security.JAASRole;
import org.mik.first.domain.security.JAASUser;
import org.mik.first.repository.CompanyRepository;
import org.mik.first.repository.CountryRepository;
import org.mik.first.repository.PersonRepository;
import org.mik.first.repository.RoleRepository;
import org.mik.first.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

public class HibernateUtil {

    private final static Logger LOG  = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory==null) {
            try {
                Configuration configuration = new Configuration();
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "org.hsqldb.jdbc.JDBCDriver");
                settings.put(Environment.URL, "jdbc:hsqldb:mem:db/mp;hsqldb.log_data=false");
                settings.put(Environment.USER, "sa");
                settings.put(Environment.PASS, "");
                settings.put(Environment.DIALECT, "");
                settings.put(Environment.SHOW_SQL, "false");
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.ALLOW_UPDATE_OUTSIDE_TRANSACTION, "true");
                settings.put(Environment.HBM2DDL_AUTO, "create-drop");
                settings.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, "true");
                configuration.setProperties(settings);
                configuration.addAnnotatedClass(Person.class);
                configuration.addAnnotatedClass(Client.class);
                configuration.addAnnotatedClass(Country.class);
                configuration.addAnnotatedClass(Company.class);
                configuration.addAnnotatedClass(Client.class);
                configuration.addAnnotatedClass(JAASRole.class);
                configuration.addAnnotatedClass(JAASUser.class);
                configuration.addAnnotatedClass(Company.class);
                configuration.addAnnotatedClass(Person.class);
                java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                initDb();
            }
            catch (Exception e) {
                LOG.error(e);
                System.exit(-1);
            }
        }
        return sessionFactory;
    }

    private static void initDb() throws Exception {
        initSecurity();
        initCountries();
        initPerson();
        initCompany();
    }

    private static void initSecurity() throws  Exception {
        createRole("user");
        createRole("api");
        createUser("joe", "the_killer_admin", "api", "user");
        createUser("jack", "the_ripper", "api");
    }

    private static void createUser(String name, String password, String ... roleNames) throws Exception {
        UserRepository userRepository = UserRepository.getInstance();
        RoleRepository roleRepository = RoleRepository.getInstance();

        List<JAASRole> JAASRoles = new ArrayList<>(roleNames.length);
        for (String r:roleNames)
            JAASRoles.addAll(roleRepository.getByName(r));

        JAASUser JAASUser = new JAASUser(name, password);
        JAASUser.setJAASRoles(JAASRoles);
        userRepository.save(JAASUser);
    }

    private static void createRole(String name) throws Exception {
        RoleRepository roleRepository = RoleRepository.getInstance();
        JAASRole JAASRole = new JAASRole(name);
        roleRepository.save(JAASRole);
    }

    private static void initCountries() throws Exception {
        createCountry("Hungary", "H");
        createCountry("USA", "US");
        createCountry("Russia", "RU");
        createCountry("Ukraine", "U");
        createCountry("Pakistan", "Pk");
        createCountry("Betelgeuse", "Bt");
        createCountry("Earth", "ETH");
        createCountry("Japan", "JP");
        createCountry("Indonesia", "ID");
        createCountry("Nigeria", "NG");
        createCountry("Mexico", "MX");
    }

    private static void createCountry(String name, String sign) throws Exception {
        CountryRepository cr = CountryRepository.getInstance();
        Country c = new Country(name, sign);
        cr.save(c);
    }

    private static void initPerson() throws Exception {
        createPerson("Zaphod Beeblebrox", "Betelgeuse", "123456789012", "Bt");
        createPerson("Ford Prefect", "Betelgeuse", "123456789021", "Bt");
        createPerson("Arthur Dent", "Earth", "321456789012", "ETH");
        createPerson("Tricia McMillan", "Earth", "543256789012", "ETH");
    }
    
    private static void createPerson(String name, String address, String idNumber, String countrySign) throws  Exception {
        PersonRepository pr = PersonRepository.getInstance();
        Country c = CountryRepository.getInstance().getBySign(countrySign);
        Person p = new Person(null, name, address, idNumber, c);
        pr.save(p);
    }
    
    private static void initCompany() throws Exception {
        createCompany("Google", "San Francisco", "13131311313131", "US");
        createCompany("Apple", "Cupertino", "31131311313131", "US");
        createCompany("KGB", "Moscow", "54131311313131","RU");
    }
    
    private static void createCompany(String name, String address, String taxNumber, String countrySign) throws Exception {
        CompanyRepository cr = CompanyRepository.getInstance();
        Country c = CountryRepository.getInstance().getBySign(countrySign);
        Company cpy = new Company(null, name, address, taxNumber, c);
        cr.save(cpy);
    }
}
